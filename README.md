# Sujet choisi : BLOG

lien du projet Gitlab : https://gitlab.com/sylv12345/django-blog-agora

# Agora

Grande place publique dans la Grèce antique. Le but de se projet est de remettre cette place au gout du jour en créant un
site où les utulisateurs posent des questions sur des sujets divers, et d'autres utilisateurs peuvent y répondre.

## Mise en place VENV

```
pythonX.X -m venv venv
```

```
$ source venv/bin/activate
```

```
$ pip install -r requirements.txt
```

## Lancement projet

```
$ python manage.py  makemigrations 
```

```
$ python manage.py  migrate 
```

```
$ python manage.py runserver
```

## Creation de l'utilisateur root pour /admin 
``` 
$ python manage.py createsuperuser 
```

## Panel admin

username : 'root'

password : 'root'

## Fonctionnalités

- creation d'un compte
- connexion/deconnexion
- creation/modifications/suppression d'un post
- creation/modifications/suppression de commentaires

